// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig:{
    apiKey: "AIzaSyAWr4pclnWiryaeSva2VpHu1-CyPzDfDv4",
    authDomain: "meteo-app-1234.firebaseapp.com",
    projectId: "meteo-app-1234",
    storageBucket: "meteo-app-1234.appspot.com",
    messagingSenderId: "881657597611",
    appId: "1:881657597611:web:f5f03243fc4a7f4ce231ee"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
