import { Injectable } from '@angular/core';

export type MeteoData = {
    temp: number,
    tempmax: number,
    tempmin: number,
    name: string,
    weather: string,
    country: string,
    wind: number,
    icon: string,
    map: string
}

@Injectable({
    providedIn: 'root'
})
export class ServizioMeteoService {
    async getData(name: string): Promise<MeteoData | null> {
        try {
            const dati = await (await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${name}&units=metric&appid=0b89b56505f1722e3eaeaa147ec72618&lang=it`)).json();
            return {
                temp: dati.main.temp,
                tempmax: dati.main.temp_max,
                tempmin: dati.main.temp_min,
                name: dati.name,
                weather: dati.weather[0].description,
                country: dati.sys.country,
                wind: dati.wind.speed,
                map: `https://www.google.com/maps/place/${dati.coord.lat},${dati.coord.lon}`,
                icon: `http://openweathermap.org/img/wn/${dati.weather[0].icon}@4x.png`
            };
        }
        catch(e) {
            alert("Città non trovata");
            return null;
        }
    }
}