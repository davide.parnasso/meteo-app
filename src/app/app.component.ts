import { Component } from '@angular/core';
import { ServizioMeteoService, MeteoData } from './services/servizio-meteo.service';
import { AngularFirestore } from '@angular/fire/compat/firestore';

type FavoriteCollection = {
    favorites: string[];
}

@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.css"]
})
export class AppComponent {
    private readonly meteo: ServizioMeteoService;
    private readonly profileName: string;
    public readonly data: { search: MeteoData[], favorites: MeteoData[] } = {search: [], favorites: []};
    public favorites: string[] = [];
    public activePage: "search" | "favorites" = "favorites";

    constructor(private firestore: AngularFirestore) {
        this.meteo = new ServizioMeteoService();
        this.profileName = this.getProfileName();
        this.loadFirebaseFavorites();
    }

    private getProfileName(): string {
        let profileName = localStorage.getItem("profileName") ?? "";

        while (profileName.trim() == "") {
            profileName = prompt("Inserisci un nome del profilo per sincronizzare i preferiti") ?? "";
        }

        localStorage.setItem("profileName", profileName);

        return profileName;
    }

    private loadFirebaseFavorites() {
        this.firestore.collection<FavoriteCollection>("Preferiti").doc(this.profileName).get().subscribe((snapshot) => {
            if(!snapshot) {
                return;
            }

            this.favorites = snapshot.data()?.favorites ?? [];

            this.loadFavorites();
        });
    }

    private loadFavorites() {
        this.data.favorites = [];
        this.favorites.forEach((favorite) => {
            this.search("favorites", null, favorite)
        })
    }

    public async search(target: typeof this.activePage, e: SubmitEvent | null, name?: string) {
        e?.preventDefault();
        const response = await this.meteo.getData(name || (e as any).target[0].value);
        if(response) {
            this.data[target].unshift(response);
        }

        if(e) {
            this.activePage = target;
        }
    }

    public removeCard(card: MeteoData) {
        this.data.search = this.data.search.filter(data => data != card);
    }

    public togglePage() {
        this.activePage = (this.activePage == "favorites") ? "search" : "favorites";
    }

    public toggleFavorite(name: string, toggle: boolean) {
        name = name.toLowerCase();
        this.favorites = this.favorites.filter(favorite => favorite != name);

        if(toggle) {
            this.favorites.push(name);
        }

        this.firestore.collection<FavoriteCollection>("Preferiti").doc(this.profileName).set({favorites: this.favorites});

        this.loadFavorites();
    }

    public deleteProfile() {
        if(confirm(`Sei sicuro di voler cancellare il profilo "${this.profileName}"?`)) {
            this.firestore.collection("Preferiti").doc(this.profileName).delete();
            localStorage.removeItem("profileName");
            location.reload();
        }
    }
}